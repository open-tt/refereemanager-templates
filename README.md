# Referee Manager - Templates

The referee manager is a free, open tool for managing table tennis referees and their assignments.

This project provides templates for the referee manager.

Both projects are part of the project "Open-TT" which provides open documents and applications for table tennis:

- <https://gitlab.com/open-tt>

## Versions/Changes

[changelog](changelog.md).

## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `master` branch.

## Legal stuff

### Licenses

License of the documents: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
See file [LICENSE](LICENSE).

License of the programs: GNU General Public License.
See file [COPYING](COPYING).

Which means:

- the documents are free, as long as you
	- don't make money with them
	- mention the creator
	- share derivates with the same license
- programs are free and open source
	- you can use the program as you want, even commercially
	- you can share the program as you like
	- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2016-2019 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of Open-TT: Referee Manager.

Open-TT: Referee Manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-TT: Referee Manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
