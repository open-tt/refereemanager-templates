title:	Kontaktliste Verbandsschiedsrichter
subtitle:	BTTV, Saison ${refereeManagerData.data.season.title.value}
options:	noemail
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${refereeManagerData.data.season.title.value}_BeTTV_VSR-Kontaktliste.md
outputpath: ../Listen

```{=latex}
\footnotesize

\DefTblrTemplate{contfoot-text}{default}{Weiter auf der nächsten Seite...}
\DefTblrTemplate{conthead-text}{default}{}

{
	\begin{tblr}[
		long,
		label=none,
	]{
		colspec = {llXc},
		hline{1,Z} = {1pt, solid},
		hline{2} = {.5pt, solid},
		row{even} = {Linen},
		row{1} = {preto=\scriptsize\bfseries},
		rowhead = {1},
		rowfoot = {0},
	}

Name & Kontakt & {Mitglied \\ Schiedst für} & Ausb. \\
```

<#--
	code from ../refs_bettv.ftl
	maybe sometime I can include the code but not yet
-->
<#assign association_bettv = "Verband.BeTTV" />
<#assign refs_bettv = [] />
<#list selection?filter(it -> (it.association??)) as referee>
	<#list referee.association as association>
		<#if (association.association.id == association_bettv) >
			<#assign refs_bettv += [referee] />
		</#if>
	</#list>
</#list>
<#-- /code from ../refs_bettv.ftl -->

<#assign used_status_types = [] />

<#list refs_bettv?filter(it -> it.active) as referee>
<@compress single_line=true>
<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.tableName.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`&`{=latex}
`{`{=latex}<#list referee.phoneNumber?filter(contact -> !contact.editorOnly) as phonenumber><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${phonenumber.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep>`\\`{=latex}</#sep></#list><#if referee.phoneNumber?? && (referee.phoneNumber?size > 0) && referee.EMail?? && (referee.EMail?size > 0) >`\\`{=latex}</#if><#list referee.EMail?filter(contact -> !contact.editorOnly) as email><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${email.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep>`\\`{=latex}</#sep></#list>`}`{=latex}
`&`{=latex}
`{`{=latex}<#if referee.member??><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.member.displayTitleShort.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if></#if><#if referee.reffor??>`\\`{=latex}*<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.reffor.displayTitleShort.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>*</#if>`}`{=latex}
`&`{=latex}
<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.highestTrainingLevel.type.shorttitle.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`\\`{=latex}

<#if !(used_status_types?seq_contains(referee.status)) >
	<#assign used_status_types += [referee.status] />
</#if>

</@compress>

</#list>

```{=latex}
\end{tblr}
```

**Legende:**

<#list used_status_types as statustype>
- <#if statustype.mmdmarkupstart??>${statustype.mmdmarkupstart.value}</#if><#if statustype.remark??>${statustype.remark.value}<#else>${statustype.title.value}</#if><#if statustype.mmdmarkupend??>${statustype.mmdmarkupend.value}</#if>
</#list>
