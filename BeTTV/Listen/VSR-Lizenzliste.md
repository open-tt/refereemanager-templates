title:	Lizenzliste Verbandsschiedsrichter
subtitle:	BTTV, Saison ${refereeManagerData.data.season.title.value}
options:	noemail, nosponsorlogo
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${refereeManagerData.data.season.title.value}_BeTTV_VSR-Lizenzliste.md
outputpath: ../Listen

<#list refereeManagerData.data.statusType as statustype>

# ${statustype.displayTitleShort.value}

<#if statustype.mmdmarkupstart??>${statustype.mmdmarkupstart.value}</#if>${statustype.displayTitle.value}<#if statustype.mmdmarkupend??>${statustype.mmdmarkupend.value}</#if>

<#assign ref_array = [] />
<#--
	code from ../refs_bettv.ftl
	maybe sometime I can include the code but not yet
-->
<#assign association_bettv = "Verband.BeTTV" />
<#assign refs_bettv = [] />
<#list selection?filter(it -> (it.association??)) as referee>
	<#list referee.association as association>
		<#if (association.association.id == association_bettv) >
			<#assign refs_bettv = refs_bettv + [referee] />
		</#if>
	</#list>
</#list>
<#-- /code from ../refs_bettv.ftl -->

<#list refs_bettv as referee>
	<#if (statustype.id == referee.status.id) >
		<#assign ref_array += [referee] />
	</#if>
</#list>

<#list ref_array>

```{=latex}
\footnotesize



\begin{longtable}[l]{@{}>{\RaggedRight}p{.25\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.43\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.07\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.12\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.09\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Club**`} & {\scriptsize`{=latex}**Ausb.**`} & {\scriptsize`{=latex}**Letzte**`} & {\scriptsize`{=latex}**Nächste**`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{4}{@{}r@{}}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#items as referee>
<@compress single_line=true>
<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>
${referee.tableName.value}
`&`{=latex}
<#if referee.member??>${referee.member.displayTitle.value}<#else>---</#if><#if referee.reffor??>`\newline`{=latex}*${referee.reffor.displayTitle.value}*</#if>
`&`{=latex}
${referee.highestTrainingLevel.type.shorttitle.value}
`&`{=latex}
<#if referee.lastTrainingUpdate??>${referee.lastTrainingUpdate.value?date["yyyy-MM-dd"]?string["dd.MM.yyyy"]}<#else>---</#if>
`&`{=latex}
<#if referee.nextTrainingUpdate??>${referee.nextTrainingUpdate.value?date["yyyy-MM-dd"]?string["yyyy"]}<#else>---</#if>
`\\`{=latex}
</@compress>

</#items>

```{=latex}
\end{longtable}
```

<#else>

Keine VSR.

</#list>

</#list>
