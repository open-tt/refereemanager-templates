title:		Spielorte der Vereine des BTTV
subtitle:		BTTV, Saison ${refereeManagerData.data.season.title.value}
options:    noemail
date:			  Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:   BeTTV_Spielorte.md

```{=latex}
\newcommand{\league}[1]{\section*{\textcolor{DarkOliveGreen}{#1}}}
\newcommand{\club}[1]{\subsection*{\textcolor{DarkSlateBlue}{#1}}}

\setlist[description]{style=nextline, font={\bfseries\color{gray}}, itemsep=2ex}
```

`\footnotesize`{=latex}

<#list refereeManagerData.data.usedLeagues as league>

`\league{`{=latex}${league.displayTitle.value}`}`{=latex}

</#list>
