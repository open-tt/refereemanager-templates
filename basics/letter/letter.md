Quotes Language:		german
Base Header Level:	3
latex leader:				kleinod-mmd-scrlttr2
MyOptions:					author=schiri, font=droid, layout=infospaltefett <#if filledInputData.getValueFor("options")??>, ${filledInputData.getValueFor("options")!}</#if>
Title:							${filledInputData.getValueFor("title")}
MyBetreff:					${filledInputData.getValueFor("title")}
Date:								<#setting locale="de">${filledInputData.getValueFor("date")?datetime.iso?string["d. MMMM yyyy"]}
MyAnA:							${current.fullName.value}<#if current.primaryAddress??>
MyAnB:							<#if current.primaryAddress.street??>${current.primaryAddress.street.value}</#if> <#if current.primaryAddress.number??>${current.primaryAddress.number.value}</#if>
MyAnC:							<#if current.primaryAddress.zipCode??>${current.primaryAddress.zipCode.value}</#if> <#if current.primaryAddress.city??>${current.primaryAddress.city.value}</#if></#if>
MyAnrede:						${filledInputData.getValueFor("opening")}
MyGruss:						${filledInputData.getValueFor("closing")}
MyUnterschrift:			${filledInputData.getValueFor("signature")}
MyAnlage:						<#list filledInputData.attachment as att>${att.title.value}<#sep>, </#sep></#list>
latex header:				\input{kleinod-mmd-style}
latex begin:				kleinod-mmd-begin-lttr
latex footer:				kleinod-mmd-end-lttr

${filledInputData.getValueFor("body")}
